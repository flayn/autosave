﻿// Guids.cs
// MUST match guids.h

namespace AutoSave
{
    using System;

    static class GuidList
    {
        public const string guidAutoSavePkgString = "2715fa44-d52f-4ea4-847f-c728a655c7f7";
        public const string guidAutoSaveCmdSetString = "bd97ada6-25ec-4c91-be09-448b46c1cf47";
        public const string guidToolWindowPersistanceString = "2b0a81dd-892b-4c7b-a873-63c69c0becf6";

        public static readonly Guid guidAutoSaveCmdSet = new Guid(guidAutoSaveCmdSetString);
    };
}