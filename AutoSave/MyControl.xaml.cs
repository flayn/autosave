﻿namespace AutoSave
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows.Documents;
    using System.Windows.Forms;
    using System.Windows.Threading;

    using EnvDTE;

    using EnvDTE80;

    using Microsoft.VisualStudio.Shell;

    using UserControl = System.Windows.Controls.UserControl;

    /// <summary>
    /// Interaction logic for MyControl.xaml
    /// </summary>
    public partial class MyControl : UserControl
    {
        private readonly DTE2 _dte2;
        private readonly Timer _timer = new Timer();
        Queue<string> _textBoxContent = new Queue<string>();

        public MyControl()
        {
            InitializeComponent();

            this._dte2 = Package.GetGlobalService(typeof(DTE)) as DTE2;

            this._timer.Tick += this._timer_Tick;

            this._timer.Interval = 3000;
            this._timer.Start();
        }

        private void _timer_Tick(object sender, EventArgs e)
        {

            if (this.AutoSave.IsChecked.HasValue && !this.AutoSave.IsChecked.Value)
            {
                return;
            }

            try
            {
                foreach (Document document in this._dte2.Documents)
                {
                    if (!document.Saved)
                    {
                        document.Save();

                        this.WriteToTextBox(document.Name);
                    }
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Error while saving: " + exception.Message);
            }
        }

        private void WriteToTextBox(string text)
        {
            if (this._textBoxContent.Count > 10)
            {
                this._textBoxContent.Dequeue();
            }

            this._textBoxContent.Enqueue(DateTime.Now.ToLongTimeString() + ": " + text);

            Dispatcher.BeginInvoke(
                 new Action(
                     () =>
                     {
                         this.TextBox.Text = string.Join(Environment.NewLine, this._textBoxContent);
                     }),
                 DispatcherPriority.Background);
        }
    }
}